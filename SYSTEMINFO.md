# Automox

Vendor: Automox
Homepage: https://www.automox.com/

Product: Automox
Product Page: https://www.automox.com/

## Introduction
We classify Automox into the ITSM (Service Management) domain as Automox provides the capabilities to report data and control devices, policies, and configurations.

## Why Integrate
The Automox adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Automox to support and enhance the IT service management processes. With this adapter you have the ability to perform operations such as:

- Get Server Groups
- Get Software Packages
- Get Server
- Get, or Create Policy

## Additional Product Documentation
The [API documents for Automox](https://developer.automox.com/developer-portal/)