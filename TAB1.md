# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Automox System. The API that was used to build the adapter for Automox is usually available in the report directory of this adapter. The adapter utilizes the Automox API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Automox adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Automox to support and enhance the IT service management processes. With this adapter you have the ability to perform operations such as:

- Get Server Groups
- Get Software Packages
- Get Server
- Get, or Create Policy

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
