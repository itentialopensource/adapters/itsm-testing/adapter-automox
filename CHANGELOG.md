
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:48PM

See merge request itentialopensource/adapters/adapter-automox!14

---

## 0.4.4 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-automox!12

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_19:04PM

See merge request itentialopensource/adapters/adapter-automox!11

---

## 0.4.2 [08-07-2024]

* Changes made at 2024.08.06_20:19PM

See merge request itentialopensource/adapters/adapter-automox!10

---

## 0.4.1 [08-06-2024]

* Changes made at 2024.08.06_12:54PM

See merge request itentialopensource/adapters/adapter-automox!9

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-automox!8

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_14:06PM

See merge request itentialopensource/adapters/itsm-testing/adapter-automox!7

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_15:06PM

See merge request itentialopensource/adapters/itsm-testing/adapter-automox!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:46PM

See merge request itentialopensource/adapters/itsm-testing/adapter-automox!5

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:12AM

See merge request itentialopensource/adapters/itsm-testing/adapter-automox!4

---

## 0.3.0 [12-30-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-automox!3

---

## 0.2.0 [05-23-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-automox!2

---

## 0.1.2 [03-01-2021]

- Migration to bring up to the latest foundation
	- Change to .eslintignore (adapter_modification directory)
	- Change to README.md (new properties, new scripts, new processes)
	- Changes to adapterBase.js (new methods)
	- Changes to package.json (new scripts, dependencies)
	- Changes to propertiesSchema.json (new properties and changes to existing)
	- Changes to the Unit test
	- Adding several test files, utils files and .generic entity
	- Fix order of scripts and dependencies in package.json
	- Fix order of properties in propertiesSchema.json
	- Update sampleProperties, unit and integration tests to have all new properties.
	- Add all new calls to adapter.js and pronghorn.json
	- Add suspend piece to older methods

See merge request itentialopensource/adapters/itsm-testing/adapter-automox!1

---

## 0.1.1 [09-17-2020]

- Initial Commit

See commit 9588eca

---
